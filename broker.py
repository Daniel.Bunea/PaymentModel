#!/usr/bin/env python   2
import socket
import sys
import re
# from broker import Broker
import json
import utilitar
import time


class Broker:
    def __init__(self):
        self.identity = "broker"
        self.id = '1242'
        self.pub_key, self.private_key = utilitar.generate_key()
        self.users = []
        self.sellers =[]
        self.acounts_temp_sellers = []
        self.acounts_temp_users = []
        self.cards_number = []
        self.credit = {}
        self.credit['123456'] = 100  # cont real bancar
        self.comituri =[]

    def enregistrare(self, msg):
        idU, kUn, kUd, cardU, ipU = msg
        idU, kUn, kUd, cardU, ipU = str(idU), str(kUn), str(kUd), str(cardU), str(ipU)
        if idU not in self.users:  # if [idU, cardU] not in self.users:
            self.users.append(idU)  # se adauga userul pt ziua
            self.cards_number.append(cardU)
            self.acounts_temp_users.append(self.credit[cardU])  # se adauga limita de creditare temporara
            # care poate fi modificata pana la sfarsitul zilei
        creditLim = self.acounts_temp_users[self.users.index(idU)]

        return self.sendPayword(idU, ipU, kUn, kUd, time.time(), creditLim)  # kUn,kUd n si d in k pub

    def sendPayword(self, U, ipU, kUn, kUd, exp, cred_lim):

        msg = str(self.id) + str(' , ') + str(U) + str(' , ') + str(ipU)
        msg += str(' , ') + str(self.pub_key[0]) + str(' , ') + str(self.pub_key[1])
        msg += str(' , ') + str(kUn) + str(' , ') + str(kUd) + str(' , ') + str(exp) + str(' , ') + str(cred_lim)
        # semnare_mesaj(msg)
        return (utilitar.semnare_mesaj(msg, self.private_key))

    def reinit_platire(self):
        for i in range(len(self.users)):
            print 'se actualizeaza contul userului '+ self.users[i]+' de la '+str(self.credit[self.cards_number[i]])+ ' $ la '+str(self.acounts_temp_users[i])+ ' $.'
            self.credit[self.cards_number[i]]=self.acounts_temp_users[i]
        for i in range(len(self.sellers)):
            print 'se transfera '+str(self.acounts_temp_sellers[i])+' $ pe contul vanzatorului '+self.sellers[i]

TCP_IP = '127.0.0.1'
TCP_PORT = 5005
BUFFER_SIZE = 1024
broker = Broker()


def check_entity(data):
    msg, sender, cod = data
    if sender == 'user' and cod == 1:  #### se primeste inregistratea
        msg = re.split(' , ', msg)
        print 'se enregistreaza info de user'
        raspuns = [broker.enregistrare(msg), 'broker', 2]
        print 'se trimite un payword userului'
        raspuns = json.dumps(raspuns).encode()
        conn.send(raspuns)
    if sender == 'seller' and cod == 5:
        'se verifica un commit ...'
        if utilitar.verifCommit(msg[0]):
            if msg[0] in broker.comituri :
                print 'operatia deja a fost efectuata !'
                return
            broker.comituri.append(msg[0])
            print 'Commit authentic si data nu este expirata, se verifica lantul...'
            msg_clar = re.split(' , ', msg[0])
            c0 = msg_clar[-3]
            lungimea = int(msg_clar[2])
            cCurent = msg_clar[1]
            print c0, lungimea, cCurent
            for i in range(lungimea):
                cCurent = utilitar.SHA.new(cCurent).hexdigest()
            if cCurent == c0:
                print 'lant ok, se verifica daca sunt destui bnai in contul utilizatorului...'
                lim_cred = msg_clar[7]
                id_U = msg_clar[2]
                if lungimea * 0.01 < lim_cred:
                    print 'credit suficient adaptam limita de creditare temporara a userului'
                    broker.acounts_temp_users[broker.users.index(id_U)] -= lungimea * 0.01
                    if msg_clar[0] not in broker.sellers :
                        broker.sellers.append(msg_clar[0])
                        broker.acounts_temp_sellers.append(lungimea * 0.01)
                    else :
                        broker.acounts_temp_sellers[broker.sellers.index(msg_clar[0])]+=lungimea * 0.01
                else:
                    print 'credit insuficient platirea refuzata !'
        else:
            'Commit neautentic sau data expirata !'


s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((TCP_IP, TCP_PORT))
s.listen(3)

conn, addr = s.accept()

print ("Connection address:", addr)
start_day = time.time()
while 1:

    if time.time() - start_day > 2 * 24:
        broker.reinit_platire()
        start_day = time.time()
    data_received = conn.recv(BUFFER_SIZE)
    sys.stdout.flush()
    data_received = data_received.decode()
    print data_received
    data_formated = json.loads(data_received)
    print 'mesaj receptionat ! '
    #print data_formated
    try:
        check_entity(data_formated)
    finally:
        pass

conn.close()


### Brokerul si vanzatorul stau pe server
