#!/usr/bin/env python   2
import socket
import sys
import re
#from broker import Broker
import json
import utilitar
import time

class Seller:
    def __init__(self):
        self.identity = "seller"
        self.id = '1242'
        self.pub_key, self.private_key = utilitar.generate_key()
        self.comituri = []
        self.c_curent = []
        self.lungime = []
        self.valid = []

    def send_commit(self):
        print 'se trimite comit'
        TCP_PORT_BROKER = 5005
        sBrok = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sBrok.connect((TCP_IP, TCP_PORT_BROKER))
        for i in range(len(seller.comituri)) :
            commit = re.split(' , ',seller.comituri[i])
            cn = seller.c_curent
            lungime = (commit[-1])
            commit_plus = [seller.comituri[i],cn,lungime]
            commit_plus = json.dumps([commit_plus, 'seller', 5])
            sBrok.send(commit_plus.encode())
        conn.close()



TCP_IP = '127.0.0.1'
TCP_PORT = 5006
BUFFER_SIZE = 1024
seller = Seller()


s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((TCP_IP, TCP_PORT))
s.listen(3)

conn, addr = s.accept()


def check_entity(data):
    msg, sender, cod = data
    if sender == 'user' and cod == 3:
        print'se verifica un commit ...'
        if utilitar.verifCommit(msg):
            print 'Commit authentic si data nu este expirata, se intreaba restul lantului a utilizatorului.'
            commit = msg
            msg = re.split(' , ', msg[0])
            seller.comituri.append(commit)
            seller.c_curent.append(msg[-3])
            seller.lungime.append(msg[-1])
            seller.valid.append(False)
            raspuns = [['trimite lant !'],seller.identity,6]
            raspuns = json.dumps(raspuns).encode()
            conn.send(raspuns)

        else:
            print 'Commit neautentic sau data expirata !'
    if sender == 'user' and cod == 7 :
        commit = msg[0]
        c_receptionat = msg[1]
        if commit in seller.comituri :
            index_comit = seller.comituri.index(commit)
            if (utilitar.SHA.new(c_receptionat).hexdigest()) == seller.c_curent[index_comit] :
                seller.c_curent[index_comit] = c_receptionat
                seller.lungime[index_comit]-=1
                print seller.lungime[index_comit]
                if seller.lungime[index_comit]==0 :
                    seller.valid[index_comit] = True
    return

print ("Connection address:", addr)
start_day = time.time()
while 1:
       sys.stdout.flush()
       data_received = conn.recv(BUFFER_SIZE)
       sys.stdout.flush()
       data_received = data_received.decode()
       print data_received
       data_formated = json.loads(data_received)
       print 'mesaj receptionat ! '
       try:
              check_entity(data_formated)
              if time.time() - start_day > 12:
                  print 'ziua over'
                  conn.close()
                  seller.send_commit()
                  start_day = time.time()
                  s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                  s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
                  s.bind((TCP_IP, TCP_PORT))
                  s.listen(3)
                  conn, addr = s.accept()
       finally:
              pass


conn.close()


### Brokerul si vanzatorul stau pe server
