 #!/usr/bin/env python   2
import socket
import sys
import json
import datetime
import random
import utilitar
import time


class User():

    def __init__(self):
        self.identity = 'user'
        self.id = '12321'
        self.pub_key, self.private_key = utilitar.generate_key()
        self.id_card = '123456'
        self.payword= ''
        self.lant = []

TCP_IP = '127.0.0.1'
TCP_PORT_BROKER = 5005
TCP_PORT_SELLER = 5006
BUFFER_SIZE = 1024
user = User()

sBrok = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sBrok.connect((TCP_IP, TCP_PORT_BROKER))

sSel = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sSel.connect((TCP_IP, TCP_PORT_SELLER))


print 'Scenario : Sunt pe Websitul venzatorului "seller1" si vreau sa cumpar ceva la 0.08$'
print 'se trmite info de user'

info_bank = json.dumps([user.id +' , '+str(user.pub_key[0])+' , '+str(user.pub_key[1])+' , '+ user.id_card+' , '+socket.gethostbyname(socket.gethostname()),user.identity,1])
sBrok.send(info_bank.encode())
rand_vanzator = False

def check_entity(data):
     msg, sender, cod = data
     if sender == 'broker' and cod == 2:
         print 'se verifica un payword ...'
         if utilitar.verifCertif(msg):
             print 'payword valid, se genereaza lantu si se trimite commit vanzatorului'
             user.payword = msg
             cn = str(random.randint(1,100000000))
             c0 = cn
             user.lant.append(c0)
             lant_size = 8
             for i in range(lant_size):
                 c0 = utilitar.SHA.new(c0).hexdigest()
                 user.lant.append(c0)
             commit = [utilitar.formare_commit('seller1', msg, c0, time.time(), lant_size, user.private_key),
                       user.identity, 3]
             commit = json.dumps(commit)
             sSel.send(commit.encode())
             global rand_vanzator
             rand_vanzator = True

         else:
             print 'payword neautentic'

     if sender == 'seller' and cod == 6:
        print 'trimitere lantului'
        for c in user.lant :
            time.sleep(1)
            c = json.dumps([c,user.identity,7])
            sSel.send(c.encode())
        #sSel.close()


     return False

while(1) :

    if rand_vanzator:
        data_received = sSel.recv(BUFFER_SIZE)
        sys.stdout.flush()
    else :
        data_received = sBrok.recv(BUFFER_SIZE)
        sys.stdout.flush()
    print 'mesaj receptionat ! '
    sys.stdout.flush()
    data_received = data_received.decode()
    print data_received
    data_formated = json.loads(data_received)

    try:
        check_entity(data_formated)

    finally:
        pass

"""
print ("RASPUNS: ", data)


print("genereaza lantul de plata")


#### se face angajamentul cu vanzatorul
sBrok.send(ANGAJAMENT.encode())

data = sBrok.recv(BUFFER_SIZE)
sys.stdout.flush()

if data == "am verificat si e ok":
    print("ma pregatesc sa trimit plata")
else:
    print(".......")


### se executa plata
sBrok.send(PLATA.encode())
"""
sBrok.close()







