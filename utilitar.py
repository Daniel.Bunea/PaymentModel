import Crypto
from Crypto.Util import asn1
from Crypto.Util import number
from fractions import gcd
from Crypto.Hash import SHA
import random
import binascii
import re
import socket
import sys
import os
__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))


def multiplicative_inverse(e, fi):
    d = 0
    x1 = 0
    x2 = 1
    y1 = 1
    temp_fi = fi

    while e > 0:
        temp1 = temp_fi / e
        temp2 = temp_fi - temp1 * e
        temp_fi = e
        e = temp2

        x = x2 - temp1 * x1
        y = d - temp1 * y1

        x2 = x1
        x1 = x
        d = y1
        y1 = y

    if temp_fi == 1:
        return d + fi


def generate_key():
    p_length = 8
    p = number.getPrime(p_length)
    q = number.getPrime(p_length)
    n = p * q
    fi = (p - 1) * (q - 1)
    e = random.randrange(1, fi)
    g = gcd(e, fi)  # verifica daca e si fi sunt coprime
    while g != 1:
        e = random.randrange(1, fi)
        g = gcd(e, fi)

    d = multiplicative_inverse(e, fi)

    # print p,q,n,fi,e,d
    return ([n, e], [n, d])

def string_bin(string): #mi a luat 8 ore cheste asta ...
    return ''.join(format(ord(c), '08b') for c in string) #formatare octets

def bin_string(binary):
    s=''
    for i in range(len(binary)/8):
        s+= chr(int(binary[i*8:(i+1)*8],2))
    return s



def semnare_mesaj(mesaj,priKey):
    mesaj_basa = SHA.new(mesaj).hexdigest()#se face hash
    prea_mare=True
    #print (mesaj_basa)
    buffer_size=len(mesaj_basa)
    #gasirea lungimea de buffer pentru mesaj
    while(prea_mare):
        binMsg = string_bin(mesaj_basa[:buffer_size])
        decMsg= int(binMsg, 2)
        #print (decMsg)
        if decMsg <= priKey[0] :
            prea_mare = False # am gasit lungimea buffurului a.i. m <= n
        else :
            buffer_size-=1

    #print (buffer_size)
    mesaj_semnat=[]
    seek=0 #loc din mesaj unde incepam sa citim
    while(seek<len(mesaj_basa)):
        if seek+buffer_size>=len(mesaj_basa):
            binMsg =string_bin(mesaj_basa[seek:])
        else :
            binMsg =string_bin(mesaj_basa[seek:seek+buffer_size])
        seek+=buffer_size
        #print(binMsg)
        decMsg= int(binMsg, 2)
        ###print decMsg
        semn = pow(decMsg,priKey[1],priKey[0])
        #print (seek)
        mesaj_semnat.append(str(semn))
    #print priKey
# format msg ex: [attack at down,[18075L, 51304L, 49540L, 33884L, 32310L, 40465L, 13345L]]
    return(mesaj,mesaj_semnat)


def verifCertif(msg):

    mesaj_clar = msg[0]
    #print SHA.new(mesaj_clar).hexdigest()
    continut_mesaj = re.split(' , ', mesaj_clar)
    pub_key_semnator = [int(continut_mesaj[3]), int(continut_mesaj[4])]
    mesaj_semnat = msg[1]
    mesaj_recontruit = ''
    authentic = True
    for i in range(len(mesaj_semnat)):
        bucat_semnat = mesaj_semnat[i]  # se va verifica fiecare bucata de mesaj

        dec_msg = pow(int(bucat_semnat), pub_key_semnator[1], pub_key_semnator[0])  # m = s**e % n
        ############print dec_msg
        bin_msg = bin(dec_msg)
        nr_bits = len(bin_msg) - 2
        if nr_bits % 8 != 0:
            bin_msg = bin_msg[2:].zfill(nr_bits + 8 - nr_bits % 8)
        else:
            bin_msg = bin_msg[2:]
        #print (bin_msg)
        str_msg = bin_string(bin_msg)
        #print (str_msg)

        mesaj_recontruit += str_msg  # se reface h(m) din sig(h(m)) cu m = s**e % n
    if mesaj_recontruit != SHA.new(mesaj_clar).hexdigest():
        authentic = False
        print(mesaj_recontruit)
        print SHA.new(mesaj_clar).hexdigest()

    return authentic

def formare_commit(idV,payword,c0,date,lungime_lant,priKey):
    payword_clar=payword[0]
    payword_semnat=payword[1]
    msg=str(idV)+str(' , ')+str(payword_clar)+str(' , ')
    for bucat_semnat in payword_semnat:
        msg+=bucat_semnat+str(' , ')
    msg+=str(c0)+str(' , ')+str(date)+str(' , ')+str(lungime_lant)
    return semnare_mesaj(msg,priKey)


def verifCommit(msg):
    mesaj_clar=msg[0]
    continut_mesaj = re.split(' , ',mesaj_clar)
    #print mesaj_clar
    # aici nu e bine, trebuie c data curenta (ataca posibila pt ca serv reseteaza la sfarsitul zilei)
    if float(continut_mesaj[len(continut_mesaj)-2])-float(continut_mesaj[8]) > 3600*24:
        print float(continut_mesaj[len(continut_mesaj) - 2]) - float(continut_mesaj[8])
        print "data este expirata, intrebati un nou payword brokerului"
        return False #data a expirat
    payword=continut_mesaj[1:len(continut_mesaj)-3] # formataj
    s =''
    for string in payword[:8]:
        s+=string+' , '
    s+=payword[8]
    payword=[s,payword[9:]]
    if verifCertif(payword):
        pub_key_U = [int(continut_mesaj[6]),int(continut_mesaj[7])]
        #print pub_key_U
        mesaj_semnat=msg[1]
        mesaj_recontruit=''
        for i in range(len(mesaj_semnat)):
            bucat_semnat = mesaj_semnat[i] #se va verifica fiecare bucata de mesaj
            #print bucat_semnat
            dec_msg = pow(int(bucat_semnat),pub_key_U[1],pub_key_U[0]) #m = s**e % n
            #####print dec_msg
            bin_msg = bin(dec_msg)
            nr_bits = len(bin_msg)-2
            if nr_bits % 8 != 0 :
                bin_msg = bin_msg[2:].zfill(nr_bits+8-nr_bits%8)
            else :
                bin_msg = bin_msg[2:]
            str_msg = bin_string(bin_msg)

            mesaj_recontruit+=str_msg # se reface h(m) din sig(h(m)) cu m = s**e % n
        if mesaj_recontruit != SHA.new(mesaj_clar).hexdigest() :
            print(mesaj_recontruit)
            print SHA.new(mesaj_clar).hexdigest()
            return False
        return True
    else :
        return False